import java.io.*;
import java.util.*;

public class majorElement {
	private static final String FILENAME = "abc.txt";
	private static final String DELIMITER = ",";	

	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line = reader.readLine()) != null){
			line = line.trim();
			findMajorElement(line);
		}

	}
	
	private static void findMajorElement(String ln){
		String[]      setOfValuesStr;
		List<Integer> setOfValues;
		final double  majorElement;
		int           currentValue;
		double        count;
		boolean       majorElementFound = false;
		
		
		setOfValuesStr = getValues(ln);
		majorElement   = setOfValuesStr.length/2.0;
		
		setOfValues    = getUniqueValues(setOfValuesStr);
		
		for (Integer tmp: setOfValues) {
			count = 0;
			majorElementFound = false;
			currentValue = tmp.intValue();
			
			for (int j = 0; j < setOfValuesStr.length; j++) {
				if(currentValue == Integer.parseInt(setOfValuesStr[j])){
					count++;
				}
			}
			
			if(count > majorElement){
				System.out.println(currentValue);
				majorElementFound = true;
				break;
			}
		}
		
		if(majorElementFound == false){
			System.out.println("None");
		}
		
	}
	
	private static String[] getValues(String ln){
		return ln.split(DELIMITER);
	}
	
	private static List<Integer> getUniqueValues(String[] arr){
		List<Integer> uniqueVals = new ArrayList<Integer>();
		
		for (int i = 0; i < arr.length; i++) {
			if(i == 0){
				uniqueVals.add(Integer.parseInt(arr[i]));
			}
			else{
				if(!uniqueVals.contains(Integer.parseInt(arr[i]))){
					uniqueVals.add(Integer.parseInt(arr[i]));
				}
			}
		}
		return uniqueVals;
	}

}