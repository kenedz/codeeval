import java.io.*;
import java.util.*;

public class Details {
	private static final String FILENAME = "abc.txt";
	private static final String DELIMITER = ",";

	public static void main(String[] args) throws IOException {
		File file = new File(FILENAME);
		Scanner in = new Scanner(file);
		int numOfTurns = 0;
		
		while(in.hasNext()){
			String[] splitArr = in.nextLine().split(DELIMITER);
			numOfTurns = makeDetails(splitArr);
			System.out.println(numOfTurns);
		}

	}
	
	private static int makeDetails(String[] arr){
		int result = 0;
		int tmp = 0;
		
		for (int i = 0; i < arr.length; i++) {
			tmp = 0;
			for (int j = 0; j < arr[0].length(); j++) {
				if(arr[i].charAt(j) == 'Y'){
					while(arr[i].charAt(--j) != 'X'){
						if(i == 0){
							result++;
						}
						else{
							tmp++;	
						}
					}
					break;
				}
			}
			
			if(result > tmp && i > 0){
				result = tmp;
			}
		}
		
		return result;
	}

}
