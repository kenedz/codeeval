import java.io.*;
import java.util.*;

public class Main{
	public static void main(String[] args) throws IOException{
		File file = new File("abc.txt");
		Scanner in = new Scanner(file);
		String line;
		try{
			while(in.hasNext()){
				line = in.nextLine();
				line = line.trim();
				findDigits(line);
			}
		}
		finally{
			in.close();
		}
	}
	
	private static void findDigits(String ln){
		StringBuffer buffer = new StringBuffer();
		final int A_CHAR = 97;
		for(int i = 0; i < ln.length(); i++){
			char letter = ln.charAt(i);
			if(isLowercaseChar(letter)){
				int hidden_num = ((int)letter - A_CHAR);
				buffer.append(String.valueOf(hidden_num));
			}
			
			if(isNumber(letter)){
				buffer.append(String.valueOf(letter));
			}
		}
		
		printResult(buffer);
	}
	
	private static boolean isLowercaseChar(char letter){
		return letter >= 'a' && letter <= ('j');
	}
	
	private static boolean isNumber(char letter){
		return letter >= '0' && letter <= '9';
	}
	
	private static void printResult(StringBuffer buff){
		if(buff.length() > 0){
			System.out.println(buff.toString());
		}
		else{
			System.out.println("NONE");
		}
	}
}