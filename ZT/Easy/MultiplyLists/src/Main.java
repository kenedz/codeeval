import java.io.*;
import java.util.*;

public class Main{
   private static String FILENAME = "abc.txt";
   private static String VERT_BAR_DELIMITER = "\\|";
   private static String SPACE_DELIMITER = " ";
   
   public static void main(String[] args) throws IOException{
      File file = new File(FILENAME);
      Scanner in = new Scanner(file);
      String line = null;
      
      try{
         while(in.hasNext()){
            line = in.nextLine();
            line = line.trim();
            run(line);
         }  
      }
      catch(Exception e){
         System.err.println("File error: " + e);
      }
      finally{
         in.close();
      }
   }
   
   private static void run(String ln){
      String[] fields = ln.split(VERT_BAR_DELIMITER);
      final int ROWS = fields.length;
      final int COLS = fields[0].split(SPACE_DELIMITER).length;
      
      String[][] matrix = new String[ROWS][COLS];
      int[][] intMtx = new int[ROWS][COLS];
      int result = 1;
      StringBuffer strResult = new StringBuffer();

      for(int i = 0; i < ROWS; i++){
         matrix[i] = fields[i].trim().split(SPACE_DELIMITER);
      }
      
      for(int i = 0; i < ROWS; i++){
         for(int j = 0; j < COLS; j++){
            intMtx[i][j] = Integer.valueOf(matrix[i][j]);
         }
      }
      
      for(int i = 0; i < COLS; i++){
         for(int j = 0; j < ROWS; j++){
            result = result * intMtx[j][i];
         }
         strResult.append(result + " ");
         result = 1;
      }
      System.out.println(strResult.toString().trim());
   }
}