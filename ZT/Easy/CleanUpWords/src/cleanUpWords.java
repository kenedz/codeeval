import java.io.*;

public class cleanUpWords {
    private static final String FILENAME = "abc.txt";
	
	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line = reader.readLine()) != null){
			line = line.trim();
			performCleanUp(line);
		}

	}
	
	private static void performCleanUp(String ln){
		StringBuffer sentence = new StringBuffer();
		boolean isEndOfWord = false;
		
		ln = ln.toLowerCase();
		
		for (int i = 0; i < ln.length(); i++) {
			if(ln.charAt(i) >= 'a' && ln.charAt(i) <= 'z'){
				sentence.append(ln.charAt(i));
				
				if(isEndOfWord != true){
					isEndOfWord = true;
				}
			}
			else{
				if(isEndOfWord == true){
					sentence.append(' ');
					isEndOfWord = false;
				}
			}
		}
		
		System.out.println(sentence.toString().trim());
	}

}
