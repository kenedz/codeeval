import java.io.*;
import java.util.*;

public class Main {
	private static final String FILENAME = "abc.txt";
	private static final String DELIMITER = " ";

	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		Scanner in = new Scanner(file);
		String line;
		
		while(in.hasNext()){
			line = in.nextLine();
			line.trim();
			comparePoints(line);
		}
	}
	
	private static void comparePoints(String ln){
		//get coordinates O,P,Q,R
		String[] arr = initArray(ln);
		String direction;
		int X = 0;
		int Y = 0;
		
		// X = Q - O
		X = Integer.parseInt(arr[2]) - Integer.parseInt(arr[0]);
		// Y = R - P
		Y = Integer.parseInt(arr[3]) - Integer.parseInt(arr[1]);
		
		direction = getDirection(X, Y);
		System.out.println(direction);
	}
	
	private static String[] initArray(String ln){
		return ln.split(DELIMITER);
	}
	
	private static String getDirection(int x, int y){
		String dir = Integer.signum(x) + "," + Integer.signum(y);
		String result;
		
		switch(dir){
		   case "1,1":
			   result = "NE";
			   break;
		   case "-1,1":
			   result = "NW";
			   break;
		   case "1,-1":
			   result = "SE";
			   break;
		   case "-1,-1":
			   result = "SW";
			   break;
		   case "1,0":
			   result = "E";
			   break;
		   case "0,1":
			   result = "N";
			   break;
		   case "0,-1":
			   result = "S";
			   break;
		   case "-1,0":
			   result = "W";
			   break;
		   default:
			   result = "here";
			   break;
		}
		return result;
	}

}
