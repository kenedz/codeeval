package def;

import java.util.*;
import java.io.*;

public class Main{
   private static final String FILENAME = "abc.txt";
   private static final String DELIMITER = " ";
   
   public static void main(String[] args) throws IOException{
      File file = new File(FILENAME);
      Scanner in = new Scanner(file);
      String line = null;
      
      try{
         while(in.hasNext()){
            line = in.nextLine();
            line = line.trim();
            run(line);
         }
      }
      catch(Exception e){
         System.err.println(e);
      }
      finally{
         in.close();
      }
   }
   
   private static void run(String ln){
      String[] array = ln.split(DELIMITER);
      Map<Integer, Integer> map = new HashMap<Integer, Integer>();
      int num;
      int min = Integer.MAX_VALUE;
      boolean found = false;
      
      for(String obj: array){
         num = Integer.valueOf(obj);
         if(map.containsKey(num)){
            map.put(num, map.get(num) + 1);
         }
         else{
            map.put(num, 1);
         }
      }
      
      for(int unique: map.keySet()){
         if(map.get(unique) == 1){
            if(min > unique){
               min = unique;
               found = true;
            }
         }
      }
      
      if(found){
         int count = 0;
         for(String str: array){
            count++;
            if(min == Integer.valueOf(str)){
               System.out.println(count);
            }
         }
      }
      else{
         System.out.println(0);
      }
   }
}