import java.util.*;
import java.io.*;

public class Main{
   private static final String FILENAME = "abc.txt";
   
   public static void main(String[] args) throws IOException{
      File file = new File(FILENAME);
      Scanner in = new Scanner(file);
      String line = null;
      
      try{
          while(in.hasNext()){
         	 line = in.nextLine();
              line = line.trim();
              run(line);
           }  
      }
      finally{
    	  in.close();
      }
   }
   
   private static void run(String ln){
	  final int SWITCHCASE = 32;
      char current;
      char[] result = new char[ln.length()];
      
      for(int i = 0; i < ln.length(); i++){
         current = ln.charAt(i);
         if(current >= 'A' && current <= 'Z'){
        	 result[i] = (char) (current + SWITCHCASE);
         }
         else if(current >= 'a' && current <= 'z'){
        	 result[i] = (char)(current - SWITCHCASE);
         }
         else{
        	 result[i] = current;
         }
      }
      
      System.out.println(result);
   }
}