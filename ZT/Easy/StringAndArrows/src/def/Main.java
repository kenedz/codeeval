package def;

import java.util.*;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Main{
   private static final String FILENAME = "abc.txt";
   private static final String LEFT_ARROW = "(?=<--<<).";
   private static final String RIGHT_ARROW = "(?=>>-->).";
   
   public static void main(String[] args) throws IOException{
      File file = new File(FILENAME);
      Scanner in = new Scanner(file);
      String line = null;
      
      try{
         while(in.hasNext()){
            line = in.nextLine();
            line = line.trim();
            run(line);
         }
      }
      catch(Exception e){
         System.err.println(e);
      }
      finally{
         in.close();
      }
   }
   
   private static void run(String ln){
      int count = 0;
      boolean found = false;
      Pattern leftArrow = Pattern.compile(LEFT_ARROW);
      Pattern rightArrow = Pattern.compile(RIGHT_ARROW);
      Matcher mLeftArrow = leftArrow.matcher(ln);
      Matcher mRightArrow = rightArrow.matcher(ln);
      
      while(mLeftArrow.find()){
         if(!found){
            found = true;
         }
         count++;
         //System.out.println(mLeftArrow.group()+ " " + mLeftArrow.start() + " " + mLeftArrow.end());
      }
      
      while(mRightArrow.find()){
         if(!found){
            found = true;
         }
         count++;
      }    
      
      if(found){
         System.out.println(count);
      }
      else{
         System.out.println(0);
      }
   }
}