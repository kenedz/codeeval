import java.util.*;
import java.io.*;

public class Main{
	final static String FILENAME = "abc.txt";
	
	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		Scanner in = new Scanner(file);
		String line;
		
		try{			
			while(in.hasNext()){
				line = in.nextLine();
				line = line.trim();
				isEven(line);
			}
		}
		finally{
			in.close();
		}
	}
	
	private static void isEven(String ln){
		int number = Integer.valueOf(ln);
		
		if((number % 2 == 0) && !(number == 0)){
			System.out.println("1");
		}
		else{
			System.out.println("0");
		}
	}
}
