package def;

import java.util.*;
import java.io.*;

public class Main{
   private static final String FILENAME = "abc.txt";
   private static final int MAX_LINE_LENGTH = 55;
   private static final int SHORT_LINE_END = 40;
   private static final String READ_STRING = "... <Read More>";
   
   public static void main(String[] args) throws IOException{
      File file = new File(FILENAME);
      Scanner in = new Scanner(file);
      String line = null;
      
      try{
         while(in.hasNext()){
            line = in.nextLine();
            line = line.trim();
            run(line);
         }
      }
      catch(Exception e){
         System.err.println(e);
      }
      finally{
         in.close();
      }
   }
   
   private static void run(String ln){
      int lineSize = ln.length();
      String newLine = null;
      
      if(lineSize > MAX_LINE_LENGTH){
         newLine = ln.substring(0, SHORT_LINE_END);
         newLine = trimEndToLastSpace(newLine);
         newLine = newLine.concat(READ_STRING);
         
         System.out.println(newLine);
      }
      else{
         System.out.println(ln);
      }
   }
   
   private static String trimEndToLastSpace(String ln){
      int end_char = ln.length() - 1;
      
      for(int i = end_char; i >= 0; i--){
         if(ln.charAt(i) == ' '){
            ln = ln.substring(0, i);
            break;
         }
      }
      return ln;
   }
}