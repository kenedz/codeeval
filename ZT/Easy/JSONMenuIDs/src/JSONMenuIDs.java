import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSONMenuIDs {
	private static final String FILENAME = "abc.txt";

	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line = reader.readLine()) != null){
			line = line.trim();
			
			if(line.length() == 0){
				continue;
			}
			
			getSumOfLabelIDs(line);
		}
	}
	
	private static void getSumOfLabelIDs(String ln) throws IndexOutOfBoundsException{
		int sum = 0;
		Pattern p = Pattern.compile("\"id\": (\\d+), \"label\"");
		Matcher m = p.matcher(ln);
		
		while(m.find()){
			sum += Integer.parseInt(m.group(1));
		}
		
		System.out.println(sum);
	}
}
