package timeToEat;
import java.io.*;
import java.util.*;

public class TimeToEat {
	private static final String DELIMITER = " ";
	private static final String COLON = ":";
	private static final String FILENAME  = "abc.txt";
	
	public static void main(String[] args) throws IOException {
		File file = new File(FILENAME);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line = reader.readLine()) != null){
			line = line.trim();
			sortTimeValues(line);
		}

	}
	
	private static void sortTimeValues(String line){
		String result;
		String[] values  = splitStringByDEL(line);
		int[] timeStamps = getSecondsArr(values);

		//sort array by insertion sort
		descendingSort(timeStamps);
		
		//convert seconds into time and get it in String
		result = getTimeFromSecsInString(timeStamps);
		
		System.out.println(result);

	}
	
	private static String[] splitStringByDEL(String ln){
		return ln.split(DELIMITER);
	}
	
	private static String[] splitStringByCOL(String ln){
		return ln.split(COLON);
	}
	
	private static int getInt(String ln){
		return Integer.parseInt(ln);
	}
	
	private static int[] getSecondsArr(String[] val){
		final int TIMECOLUMNS = 3;
		String[][] arrOfSplitTS = new String[val.length][TIMECOLUMNS];
		int[] timeInSecList = new int[val.length];
		int value = 0;
		
		for (int i = 0; i < arrOfSplitTS.length; i++) {
			arrOfSplitTS[i] = splitStringByCOL(val[i]);
		}
		
		for (int i = 0; i < arrOfSplitTS.length; i++) {
			// seconds = seconds + minutes * 60s + hours * 60mins * 60s
			value = getInt(arrOfSplitTS[i][2]) + getInt(arrOfSplitTS[i][1]) * 60 + 
					getInt(arrOfSplitTS[i][0]) * 60 * 60;
			timeInSecList[i] = value;
		}
		
		return timeInSecList;
	}
	
	private static String getTimeFromSecsInString(int[] lst){
		int value = 0;
		int hrs   = 0;
		int mins  = 0;
		int secs  = 0;
		StringBuffer allTimeValues = new StringBuffer();
		
		for (int val: lst) {
			value = val;
			//get hrs
			hrs = value / (60 * 60);
			//get mins
			value = (value - (hrs * 60 * 60));
			mins = value / 60;
			//get secs
			value = value - (mins * 60);
			secs = value;
			allTimeValues.append(String.format("%02d", hrs)
					              + ":" + String.format("%02d",mins) + ":" + 
			                     String.format("%02d",secs) + " ");
		}
		
		return allTimeValues.toString().trim();
	}
	
	//insertion sort
	public static int[] descendingSort(int[] arr){
		for (int i = 0; i < arr.length - 1; i++) {
			int j = i + 1;
			int tmp = arr[j];
			while(j > 0 && tmp > arr[j - 1]){
				arr[j] = arr[j - 1];
				j--;
			}
			arr[j] = tmp;
		}
		return arr;
	}
}
