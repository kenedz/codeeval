import java.io.*;

public class highestScore {
	private static final String FILENAME     = "abc.txt";
	private static final String DELIMITER    = " ";
	private static final String VERTICALBAR  = "\\|";

	public static void main(String[] args) throws IOException {
		File file = new File(FILENAME);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line = reader.readLine()) != null){
			line = line.trim();
			scoreArtists(line);
		}

	}
	
	private static void scoreArtists(String line){
		String[]   splitValues   = splitByVBar(line);
		String[][] finalMatrics  = splitByDelimiter(splitValues);
		final int  NUMOFSTYLES   = finalMatrics[0].length;
		int[]      maxArr        = new int[NUMOFSTYLES];
		int        mtxValue      = 0;
		
		for (int i = 0; i < finalMatrics[0].length; i++) {
			maxArr[i] = Integer.valueOf(finalMatrics[0][i]);
			for (int j = 0; j < finalMatrics.length; j++) {
				mtxValue = Integer.valueOf(finalMatrics[j][i]);
				if(maxArr[i] < mtxValue){
					maxArr[i] = mtxValue;
				}
			}
		}
		
		printArr(maxArr);
	}
	
	private static String[] splitByVBar(String ln){
		return ln.split(VERTICALBAR);
	}
	
	private static String[][] splitByDelimiter(String[] arr){
		final int NUMOFARTISTS = arr.length;
		final int NUMOFSTYLES = arr[0].trim().split(DELIMITER).length;
		String[][] mtx = new String[NUMOFARTISTS][NUMOFSTYLES];
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = arr[i].trim();
		}
		
		for (int i = 0; i < mtx.length; i++) {
			mtx[i] = arr[i].split(DELIMITER);
		}
		
		return mtx;
	}
	
	private static void printArr(int[] arr){
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < arr.length; i++) {
			result.append(String.valueOf(arr[i]) + " ");
		}
		System.out.println(result.toString().trim());
	}

}
