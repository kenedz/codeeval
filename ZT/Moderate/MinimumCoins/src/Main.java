import java.io.*;

public class Main {
	private static final String FILENAME = "abc.txt";

	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		int remaining;
		
		while((line = reader.readLine()) != null){
			remaining = Integer.parseInt(line.trim());
			
			int fives = remaining / 5;
			remaining -= fives * 5;
			
			int threes = remaining / 3;
			remaining -= threes * 3;
			
			int ones = remaining;
			remaining -= ones;
			
			System.out.println(fives + threes + ones);
		}

	}

}
