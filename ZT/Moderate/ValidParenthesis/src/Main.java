import java.util.*;
import java.io.*;

public class Main{
   private static final String FILENAME = "abc.txt";
   
   public static void main(String[] args) throws IOException{
      File file = new File(FILENAME);
      Scanner in = new Scanner(file);
      String line;
      try{
         while(in.hasNext()){
            line = in.nextLine();
            line = line.trim();
            run(line);
         }
      }
      finally{
         in.close();
      }
   }
   
   private static void run(String ln){
      if(isValid(ln)){
         System.out.println("True");
      }
      else{
         System.out.println("False");
      }
   }
   
   private static boolean isValid(String ln){
      HashMap<Character, Character> map = new HashMap<Character, Character>();
      map.put('(',')');
      map.put('{','}');
      map.put('[',']');
      
      Stack<Character> stack = new Stack<Character>(); 

      for(int i = 0; i < ln.length(); i++){
         char curr = ln.charAt(i);
         if(map.keySet().contains(curr)){
            stack.push(curr);
         }
         else if(map.values().contains(curr)){
            if(!stack.isEmpty() && map.get(stack.peek()) == curr){
               stack.pop();
            }
            else{
               return false;
            }
         }
      }
      return stack.empty();
   }
}