import java.io.*;
import java.util.*;

public class Main {
	private static final String FILENAME = "abc.txt";
	private static final String DELIMITER = ",";
	private static final char NULLCHAR = '\u0000'; 

	public static void main(String[] args) throws IOException{
		File file = new File(FILENAME);
		Scanner in = new Scanner(file);
		String line;
		
		while(in.hasNext()){
			line = in.nextLine();
			
			if(line == null){
				continue;
			}
			
			line = line.trim();		
			remove(line);
		}
	}
	
	private static void remove(String ln){
		char[] finalText;
		String[] arr = ln.split(DELIMITER); //split line into original text and characters being removed
		trimArray(arr);                     //trim all items in array
		
		finalText = removeCharacters(arr);  //remove characters defined by second item in arr
		
		printCharArray(finalText);
	}
	
	private static void trimArray(String[] arr){
		for (int i = 0; i < arr.length; i++) {
			arr[i] = arr[i].trim();
		}
	}
	
	private static char[] removeCharacters(String[] arr){
		if(arr.length != 2){
			return null;
		}
		
		char[] afterRem = new char[arr[0].length()]; 
		char[] charsToBeRemoved = arr[1].toCharArray();
		char current;
		char count = 0;
		
		for (int i = 0; i < arr[0].length(); i++) {
			current = arr[0].charAt(i);
			if(!removeCurrentChar(current, charsToBeRemoved)){
				afterRem[count] = current;
				count++;
			}
		}
		
		return afterRem;
	}
	
	private static boolean removeCurrentChar(char c, char[] rem){
		boolean toBeRemoved = false;
		for (int i = 0; i < rem.length; i++) {
			if(c == rem[i]){
				toBeRemoved = true;
				break;
			}
		}
		
		return toBeRemoved;
	}
	
	private static void printCharArray(char[] arr){
		StringBuffer text = new StringBuffer();
		
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] != NULLCHAR){
			    text.append(arr[i]);	
			}
		}
		System.out.println(text.toString());
	}

}
