import java.io.*;
import java.util.*;

//"Code" Combinations
public class Main {
	private static final String FILENAME     = "abc.txt";
	private static final String ROWDELIMITER = "\\|";
	private static final int SUBMTXROWS = 2;
	private static final int SUBMTXCOLS = 2;

	public static void main(String[] args) throws IOException{
		File file  = new File(FILENAME);
		Scanner in = new Scanner(file);
		String line;
		while(in.hasNext()){
			line = in.nextLine();
			line = line.trim();
			getSolution(line);
		}
	}
	
	private static void getSolution(String ln){		
		String[]    arr  = ln.split(ROWDELIMITER);           //get specific lines
		char[][]    mtx  = initMatrix(arr);                  //initialize default matrix
		char[][] subMtx  = new char[SUBMTXROWS][SUBMTXROWS]; //initialize submatrix
		int      count   = 0;                                //counter
		
		for (int i = 0; i < mtx.length - 1; i++) {
			for (int j = 0; j < mtx[0].length - 1; j++) {
				subMtx = initSubMtx(mtx, i, j);
				if(containsWord(subMtx)){
					count++;
				}
			}
		}
		
		System.out.println(count);
	}
	
	private static char[][] initMatrix(String[] arr){
		final int ROWS = arr.length;
		final int COLS = arr[0].length();
		char[][] mtx   = new char[ROWS][COLS];
		
		for (int i = 0; i < mtx.length; i++) {
			arr[i] = arr[i].trim();
			mtx[i] = arr[i].toCharArray();
		}
		
		return mtx;
	}
	
	private static char[][] initSubMtx(char[][] mtx, int row, int col){
		char[][] subMtx = new char[SUBMTXROWS][SUBMTXCOLS];
		
		for (int i = 0; i < SUBMTXROWS; i++) {
			for (int j = 0; j < SUBMTXCOLS; j++) {
				if(row < mtx.length - 1 && col < mtx[0].length - 1){
					subMtx[i][j] = mtx[i + row][j + col];
				}
			}
		}
		return subMtx;
	}
	
	private static boolean containsWord(char[][] mtx){
		boolean containsWordCode = false;
		String  word             = "";
		
		for (int i = 0; i < SUBMTXROWS; i++) {
			for (int j = 0; j < SUBMTXCOLS; j++) {
				word = word + mtx[i][j];
			}
		}
		if(word.contains("c") && word.contains("o") && word.contains("d") && word.contains("e")){
			containsWordCode = true;
		}
		return containsWordCode;
	}

}
