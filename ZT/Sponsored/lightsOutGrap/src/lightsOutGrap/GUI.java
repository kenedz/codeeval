package lightsOutGrap;

import java.awt.EventQueue;
import java.io.*;

import javax.swing.JFrame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class GUI {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws IOException{
		File file = new File("abc.txt");
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		lightsHandler handler = new lightsHandler();
		
		while((line = reader.readLine()) != null){
			line = line.trim();
			handler.setMatrics(line);
			
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						GUI window = new GUI(handler);
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public GUI(lightsHandler handler) {
		initialize(handler);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(lightsHandler handler) {
		final int numOfRows = handler.getNumOfRows();
		final int numOfColumns = handler.getNumOfColumns();
		char[][] bNames = handler.getMatrics();
		
		int bPosX = 10;          // position of button in axis X
		int bPosY = 10;          // position of button in axis Y
		final int bHeight = 50;  // button height
		final int bWeight = 50;  // button weight
		final int lHeight = 50;  // label height
		final int lWeight = 200; // label weight
		final int gap = 10;      // gap between components
		
		final int fHeight = numOfRows * (bHeight + gap) + lHeight + 50;
		final int fWeight = numOfColumns * (bWeight + gap) + 50;
	
		frame = new JFrame();
		frame.setBounds(100, 100, fWeight, fHeight);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		myJbutton[][] button = new myJbutton[numOfRows][numOfColumns];
		JLabel label = new JLabel();
		
		for (int i = 0; i < numOfRows; i++) {
			for (int j = 0; j < numOfColumns; j++) {
				button[i][j] = new myJbutton();
				
				button[i][j].setText(String.valueOf(bNames[i][j]));
				button[i][j].setRow(i);
				button[i][j].setColumn(j);
				
				label.setText("Num of turns: ");
				
				final int row = button[i][j].getRow();
				final int column = button[i][j].getColumn();
				
				button[i][j].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e){
						//TODO - dodelat zavolani funkce pro zmenu tlacitek
						handler.pushButton(button, row, column);
						handler.setLabel(label);
					}
				});
				button[i][j].setBounds(bPosX, bPosY, bWeight, bHeight);
				frame.getContentPane().add(button[i][j]);
				bPosX = bPosX + bHeight + gap;
			}
			bPosX = 10;
			bPosY = bPosY + bWeight + gap;
		}
		label.setBounds(bPosX, bPosY, lWeight, bHeight);
		frame.getContentPane().add(label);
	}
}
