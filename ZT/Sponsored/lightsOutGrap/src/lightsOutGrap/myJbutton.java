package lightsOutGrap;
import javax.swing.JButton;

public class myJbutton extends JButton{
	private int row;
	private int column;
	
	public void setRow(int row){
		this.row = row;
	}
	
	public void setColumn(int column){
		this.column = column;
	}
	
	public int getRow(){
		return this.row;
	}
	
	public int getColumn(){
		return this.column;
	}
}
