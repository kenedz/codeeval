package lightsOutGrap;

import javax.swing.JOptionPane;
import javax.swing.JLabel;

public class lightsHandler {
	private static final String DELIMITER = " ";
	private static final String VBAR = "\\|";
	private int numOfRows = 0;
	private int numOfColumns = 0;
	private int row = 0;
	private int column = 0;
	private String initData;
	private char[][] matrics;
	private static int count = 0;
	
	
	/* 
	 * Sets 
	 * */
	private void setNumOfRows(int row){
		this.numOfRows = row;
	}
	
	private void setNumOfColumns(int col){
		this.numOfColumns = col;
	}
	
	private void setData(String data){
		this.initData = data;
	}
	
	public void setMatrics(String ln){
		String[] splitArray = ln.split(DELIMITER);
		final int row =     loadValue(splitArray[0]);
		final int column =  loadValue(splitArray[1]);
		final String data = splitArray[2];
		
        this.setNumOfRows(row);
        this.setNumOfColumns(column);
        this.setData(data);
		this.initMatrics();
	}
	
	/*
	 * Gets
	 */
	
	public int getNumOfRows(){
		return this.numOfRows;
	}
	
	public int getNumOfColumns(){
		return this.numOfColumns;
	}
	
	public String getData(){
		return this.initData;
	}
	
	public char[][] getMatrics(){
		return this.matrics;
	}
	
	/*
	 * Other 
	 */
	
	private void initMatrics(){
		int numOfRows = this.getNumOfRows();
		int numOfcolumns = this.getNumOfColumns();
		String data = this.getData();
		String[] arrayOfData = data.split(VBAR);
		
		if(numOfRows != 0 || numOfcolumns != 0){
			this.matrics = new char[numOfRows][numOfcolumns];
		}
		else{
			System.out.println("Matrics dimensions initialized as default!");
			this.matrics = new char[1][1];
		}
		
		for (int i = 0; i < this.matrics.length; i++) {
			this.matrics[i] = arrayOfData[i].toCharArray();
		}
		
	}
	
	public int loadValue(String str){
		return Integer.valueOf(str);
	}
	
	public void pushButton(myJbutton[][] jb, int row, int col){
		char text;
		increaseCount();
		
		for (int i = 0; i < jb.length; i++) {
			for (int j = 0; j < jb[row].length; j++) {
				if((i == row && j == col - 1) ||
				   (i == row && j == col + 1) ||
				   (i == row && j == col) ||
				   (i == row - 1 && j == col) ||
				   (i == row + 1 && j == col)){
					text = jb[i][j].getText().charAt(0);
					
					if(text == '.'){
						jb[i][j].setText("O");
					}
					else if(text == 'O'){
						jb[i][j].setText(".");
					}
					else{
						return;
					}
				}
			}
		}
		
		if(isTurnedOn(jb) == false){
			JOptionPane.showMessageDialog (null, "You won in " + getCount() + " moves!", "lightsOut", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
	}
	
	public boolean isTurnedOn(myJbutton[][] jb){
		char text;
		boolean isOn = false;
		
		for (int i = 0; i < jb.length; i++) {
			for (int j = 0; j < jb[0].length; j++) {
				text = jb[i][j].getText().charAt(0);
				if(text == 'O'){
					isOn = true;
					break;
				}
			}
			if(isOn == true){
				break;
			}
		}
		return isOn;
	}
	
	private void increaseCount(){
		++this.count;
	}
	
	private int getCount(){
		return this.count;
	}
	
	public void setLabel(JLabel textField){
		textField.setText("Num of turns: " + Integer.toString(this.getCount()));
	}
	
}
