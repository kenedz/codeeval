import java.io.*;
import java.util.*;

public class Main {
	private static final String FILENAME = "abc.txt";
	private static final int ALPHABETSIZE = 256;
	
	private class Node implements Comparable<Node>{
        private final String ch;
        private final int  freq;
        private final Node left;
        private final Node right;
        
        Node(Node n){
        	this.ch = n.ch;
        	this.freq = n.freq;
        	this.left = n.left;
        	this.right = n.right;
        }
        
        Node(String ch, int freq, Node left, Node right){
        	this.ch = ch;
        	this.freq = freq;
        	this.left = left;
        	this.right = right;
        }
        
        private boolean isLeaf(){
        	return (left == null) && (right == null);
        }
		
        @Override
		public int compareTo(Node n){
			return this.freq - n.freq;
		}
        
        public String getStr(){
        	return this.ch;
        }
        
        public int getFreq(){
        	return this.freq;
        }
	}

	public static void main(String[] args) throws FileNotFoundException{
		File file = new File(FILENAME);
		Scanner in = new Scanner(file);
		try{
			String line;
			while(in.hasNext()){
				line = in.nextLine();
				line = line.trim();
				performHuffmanCoding(line);
			}
		}
		finally{
			in.close();
		}
	}
	
	private static void performHuffmanCoding(String ln){
		List<Node> Nodes = createNodes(ln); /* get list of nodes with symbols */
		
		/*sort nodes with symbols based on frequency ascendingly and by characters same way*/
		Collections.sort(Nodes);

        /* build binary tree */
		buildTree(Nodes);

		/* compress data */
		compress(Nodes);
	}
	
	private static List<Node> createNodes(String ln){
		Main huff = new Main();                     /* instance of Main */
		List<Node> Nodes = new ArrayList<Node>();   /* instance of list */
		char[] input = ln.toCharArray();            /* get char array from text */	
		int[] freq = getFrequency(input);           /* get frequency of each char */
		
		/* create nodes for each char contained in text */
		for (char i = 0; i < ALPHABETSIZE; i++) {
			if(freq[i] > 0){
				Nodes.add(huff.new Node(String.valueOf(i), freq[i], null, null));
			}
		}
		
		return Nodes;
	}
	
	private static int[] getFrequency(char[] arr){
		int[] freq = new int[ALPHABETSIZE]; /* initialize alphabet with frequency */
		
		for(int i = 0; i < arr.length; i++){
			freq[arr[i]]++;
		}
		
		return freq;
	}
	
	private static void buildTree(List<Node> list){
		Main huff = new Main();
		List<Node> temp = new ArrayList<Node>(list);
		
		while (temp.size() > 1) {
			/* add parents */
			Node left = temp.get(0);
			Node right = temp.get(1);
			Node parent = huff.new Node(left.getStr() + right.getStr(),
		                                left.getFreq() + right.getFreq(),
		                                left, right);
			list.add(parent);
			temp.add(parent);
			/* remove two processed nodes */
			temp.remove(0);
			temp.remove(0);
			
			/* sort nodes */
			sortList(temp);
		}
	}
	
	private static void sortList(List<Node> list){
		Collections.sort(list);
		Main huff = new Main();
		
		for (int i = 0; i < list.size() - 1; i++) {
			int j = i + 1;
			Node temp = huff.new Node(list.get(j));
			
			if(!temp.isLeaf()){
				/* sort till j > 0 and node move to right before symbols of same frequency 
				 * and all that alphabetically */
				while(j > 0 && temp.getFreq() == list.get(j - 1).getFreq() &&
					  list.get(j - 1).isLeaf()){
					list.set(j, list.get(j - 1));
					j--;
				}
				list.set(j, temp);
			}
		}
	}
	
	private static void compress(List<Node> list){
		Main huff = new Main();
		Node root = huff.new Node(list.get(list.size() - 1));
		String[] resArr = new String[ALPHABETSIZE];
		
		/* build binary data*/
		buildCode(resArr, root, "");
		
		printValues(resArr);
	}
	
	private static void buildCode(String[] arr, Node nd, String s){
		if(!nd.isLeaf()){
			buildCode(arr, nd.left, s + "0");
			buildCode(arr, nd.right, s + "1");
		}
		else{
			arr[nd.ch.charAt(0)] = s;
		}
	}
	
	private static void printValues(String[] arr){
		StringBuffer buffer = new StringBuffer();
		
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] != null && !arr[i].isEmpty()){
				buffer.append(String.valueOf(Character.toChars(i)) + ": " + arr[i] + "; ");
			}
		}
		System.out.println(buffer.toString().trim());
	}
}
